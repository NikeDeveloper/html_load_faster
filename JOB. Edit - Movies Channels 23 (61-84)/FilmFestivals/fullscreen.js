// JavaScript Document
/*!
* Include this javascript below on every page of your site and it basically will grab all the anchor tags on the page and it handles the window location, which will load the new page and keep you in the full screen mode.
*/


function fullscreen() {
    $('a').click(function() {
        if(!$(this).hasClass('noeffect')) {
            window.location = $(this).attr('href');
            return false;
        }
    });
}

fullscreen();



/*================================================Umair Code=======================================================================*/

// add youtube links

var links=new Array(); // regular array (add an optional integer)
var linksLentgh;
var srclink;
var i=5;

links[0]="mlkMGH9ERGY";  //  Commonwealth Bank - How to find your ideal first home
links[1]="34IPlEalgSY";  //  					Financing your home
links[2]="WiT-KZRE8L8";  //					Internet Banking - How to make payments and transfers
links[3]="8NwmXqhfod0";  //					PropertyMate - Inspections made easy
links[4]="AOu-cejQqjw";  // St George Bank - 	Budget Planner
links[5]="Vrun25eenio";  //					How to: Do you have enough money to retire?
links[6]="1Iqn2lkC1K8";  //					Your business: The skinny on cashflow
links[7]="xd6EH651jC4";  //  					Steps to investing in property
links[8]="hhRAuwF9FHU";  //  					Managing your investment property
links[9]="kIkMJyO-gHQ";  //  Westpac Banking - Your business: Cashflow forecasting
links[10]="93bR2AZyqW8";  //					Your business: Finding sources of funding








var player;

function onYouTubePlayerAPIReady() {
    player = new YT.Player('youtube', {
        height: '390',
        width: '640',
        videoId: links[i],
        playerVars: {
            'autoplay': 0,
            'controls': 0,
            'showinfo': 0,
            'rel': 0
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
            'onError': onPlayerError

        }
    });
}

//elance_22_09_14
var flag = 0;

function onPlayerReady(event) {

    //elance_22_09_14
    if (flag == 0) {
        flag = 1;
        console.log('INIT : Start Blank Screen..');
        $('#youtubecover').fadeIn(youtubecover_fadeIn_ready);
    }

    var myVar = setInterval(function() {
        if (document.readyState === "complete") {
            setTimeout(function() {
                event.target.playVideo();
            }, videoStartSecondAfterLoad);
            clearInterval(myVar);
        }
    }, 1000);
}



function videoLogic()

{
    srclink = "https://www.youtube.com/embed/" + links[0] + "?enablejsapi=1&HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";
    // change the src attribute of iframe of youtube video
    //$('#youtube').attr('src', srclink);

    document.getElementById('youtube').src = "https://www.youtube.com/embed/" + links[0] + "?enablejsapi=1&HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";

    player = new YT.Player('youtube', {
        events: {
            'onStateChange': onPlayerStateChange,
            'onError': onPlayerError
        }
    });
}


function onPlayerStateChange(event) { //detect if video is ended

    //elance_22_09_14
    console.log("YT-EVENT : " + event.data + " VIDEO : " + $('#youtube').attr('src'));
    if (event.data == 1) {
        $('#youtubecover').fadeOut(youtubecover_fadeOut_statechange);
        console.log('End Blank Screen..');
    }
    if (event.data == 0) {
        $('#youtubecover').fadeIn(youtubecover_fadeIn_statechange);
        console.log('Start Blank Screen..');
    }

    switch (event.data) {
        case YT.PlayerState.ENDED:
            i = i + 1;
            console.log("play video " + i + " of " + links.length);
            srclink = "https://www.youtube.com/embed/" + links[i] + "?HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";

            // change the src attribute of iframe of youtube video

            //$('#youtube').attr('src', srclink);
            console.log(srclink);
            // console.log('Video has ended.');

            document.getElementById('youtube').src = "https://www.youtube.com/embed/" + links[i] + "?enablejsapi=1&HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";

            player = new YT.Player('youtube', {
                events: {

                    'onStateChange': onPlayerStateChange,
                    'onError': onPlayerError
                }
            });

            if (i == links.length - 1) //break recursion on end of the array
            {
                i = 0;
            }
            break;
    }
    switch (event.data) {
        case YT.PlayerState.UNSTARTED:
            i = i + 1;
            console.log("-play video " + i + " of " + links.length);

            srclink = "https://www.youtube.com/embed/" + links[i] + "?HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";
            // change the src attribute of iframe of youtube video

            //$('#youtube').attr('src', srclink);
            // console.log('-Video has ended.');
            document.getElementById('youtube').src = "https://www.youtube.com/embed/" + links[i] + "?enablejsapi=1&HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";

            player = new YT.Player('youtube', {
                events: {
                    'onStateChange': onPlayerStateChange,
                    'onError': onPlayerError
                }
            });

            if (i == links.length - 1) //break recursion on end of the array
            {
                i = 0;
            }
    }
}

function onPlayerError(event) {
    // console.log(event.data);
    console.log("error", links[i]);
    i = (i + 1) % links.length;
    document.getElementById('youtube').src = "https://www.youtube.com/embed/" + links[i] + "?enablejsapi=1&HA=1&rel=0&showinfo=0&autohide=1&wmode=transparent&autoplay=1";
    player = new YT.Player('youtube', {
        events: {
            'onStateChange': onPlayerStateChange,
            'onError': onPlayerError
        }
    });
    //alert('Video Not Found');
}

//This function will shuffle the array
function shuffle(array) {
    var counter = array.length,
        temp, index;
    // While there are elements in the array
    while (counter--) {
       // pick a random index
       index = (math.random() * counter) | 0;
       // and swap the last element with it
       temp = array[counter];
       array[counter] = array[index];
       array[index] = temp;
    }
    return array;
}
shuffle(links); //shuffle videos
$(window).load(function() { //let the video load first
    $('#close').click(function() {
        closeWindows();
    });
    //initialize
    $('#fullscreen').click(function() {
        if (screenfull.enabled) {
            // We can use `this` since we want the clicked element
            screenfull.request();
        }
        $('#overlay, #fade').css('display', 'none');
        videoLogic();
    });
    $('#standardscreen').click(function() {
        $('#overlay, #fade').css('display', 'none');
        videoLogic();
    });
    $('.link').click(function() {
        if (screenfull.enabled) {
            // We can use `this` since we want the clicked element
            screenfull.toggle();
        }
    });
});
